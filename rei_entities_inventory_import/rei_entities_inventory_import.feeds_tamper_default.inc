<?php

/**
 * @file
 * Bulk export of feeds_tamper_default objects generated by Bulk export module.
 */

/**
 * Implements hook_feeds_tamper_default().
 */
function rei_entities_inventory_import_feeds_tamper_default() {
  $feeds_tampers = array();

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rei_inventory_import-abbr-explode';
  $feeds_tamper->importer = 'rei_inventory_import';
  $feeds_tamper->source = 'abbr';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => ',',
    'limit' => '',
    'real_separator' => ',',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode Acronym Values';
  $feeds_tampers['rei_inventory_import-abbr-explode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rei_inventory_import-access-default_value';
  $feeds_tamper->importer = 'rei_inventory_import';
  $feeds_tamper->source = 'access';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => 'unknown',
    'only_if_empty' => 1,
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Set empty Access value to Unknown';
  $feeds_tampers['rei_inventory_import-access-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rei_inventory_import-classification-explode';
  $feeds_tamper->importer = 'rei_inventory_import';
  $feeds_tamper->source = 'classification';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => ',',
    'limit' => '',
    'real_separator' => ',',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode Classification Values';
  $feeds_tampers['rei_inventory_import-classification-explode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rei_inventory_import-custodian-explode';
  $feeds_tamper->importer = 'rei_inventory_import';
  $feeds_tamper->source = 'custodian';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => ',',
    'limit' => '',
    'real_separator' => ',',
  );
  $feeds_tamper->weight = 0;
  $feeds_tamper->description = 'Explode Custodian Values';
  $feeds_tampers['rei_inventory_import-custodian-explode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rei_inventory_import-dept-explode';
  $feeds_tamper->importer = 'rei_inventory_import';
  $feeds_tamper->source = 'dept';
  $feeds_tamper->plugin_id = 'explode';
  $feeds_tamper->settings = array(
    'separator' => ',',
    'limit' => '',
    'real_separator' => ',',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Explode Department Values';
  $feeds_tampers['rei_inventory_import-dept-explode'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rei_inventory_import-published-default_value';
  $feeds_tamper->importer = 'rei_inventory_import';
  $feeds_tamper->source = 'published';
  $feeds_tamper->plugin_id = 'default_value';
  $feeds_tamper->settings = array(
    'default_value' => '1',
    'only_if_empty' => 1,
  );
  $feeds_tamper->weight = 3;
  $feeds_tamper->description = 'Set empty value as Published';
  $feeds_tampers['rei_inventory_import-published-default_value'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rei_inventory_import-published-find_replace_publish';
  $feeds_tamper->importer = 'rei_inventory_import';
  $feeds_tamper->source = 'published';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => 'Publish',
    'replace' => '1',
    'case_sensitive' => 0,
    'word_boundaries' => 0,
    'whole' => 1,
    'regex' => TRUE,
    'regex_find' => '/^Publish$/ui',
  );
  $feeds_tamper->weight = 2;
  $feeds_tamper->description = 'Replace Publish with 1';
  $feeds_tampers['rei_inventory_import-published-find_replace_publish'] = $feeds_tamper;

  $feeds_tamper = new stdClass();
  $feeds_tamper->disabled = FALSE; /* Edit this to true to make a default feeds_tamper disabled initially */
  $feeds_tamper->api_version = 2;
  $feeds_tamper->id = 'rei_inventory_import-published-find_replace_unpublish';
  $feeds_tamper->importer = 'rei_inventory_import';
  $feeds_tamper->source = 'published';
  $feeds_tamper->plugin_id = 'find_replace';
  $feeds_tamper->settings = array(
    'find' => 'unpublish',
    'replace' => '0',
    'case_sensitive' => 0,
    'word_boundaries' => 0,
    'whole' => 1,
    'regex' => TRUE,
    'regex_find' => '/^unpublish$/ui',
  );
  $feeds_tamper->weight = 1;
  $feeds_tamper->description = 'Replace Unpublish with 0';
  $feeds_tampers['rei_inventory_import-published-find_replace_unpublish'] = $feeds_tamper;

  return $feeds_tampers;
}
