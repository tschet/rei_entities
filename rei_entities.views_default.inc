<?php

/**
 * @file
 * Bulk export of views_default objects generated by Bulk export module.
 */

/**
 * Implements hook_views_default_views().
 */
function rei_entities_views_default_views() {
    $views = array();

    $view = new view();
    $view->name = 'rei_custodian_filter';
    $view->description = '';
    $view->tag = 'default';
    $view->base_table = 'users';
    $view->human_name = 'REI Custodian Filter';
    $view->core = 7;
    $view->api_version = '3.0';
    $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

    /* Display: Master */
    $handler = $view->new_display('default', 'Master', 'default');
    $handler->display->display_options['use_more_always'] = FALSE;
    $handler->display->display_options['access']['type'] = 'none';
    $handler->display->display_options['cache']['type'] = 'none';
    $handler->display->display_options['query']['type'] = 'views_query';
    $handler->display->display_options['exposed_form']['type'] = 'basic';
    $handler->display->display_options['pager']['type'] = 'full';
    $handler->display->display_options['style_plugin'] = 'entityreference_style';
    $handler->display->display_options['style_options']['search_fields'] = array(
        'field_rei_user_name' => 'field_rei_user_name',
    );
    $handler->display->display_options['row_plugin'] = 'fields';
    /* Field: User: Name */
    $handler->display->display_options['fields']['field_rei_user_name']['id'] = 'field_rei_user_name';
    $handler->display->display_options['fields']['field_rei_user_name']['table'] = 'field_data_field_rei_user_name';
    $handler->display->display_options['fields']['field_rei_user_name']['field'] = 'field_rei_user_name';
    $handler->display->display_options['fields']['field_rei_user_name']['label'] = '';
    $handler->display->display_options['fields']['field_rei_user_name']['element_label_colon'] = FALSE;
    $handler->display->display_options['fields']['field_rei_user_name']['click_sort_column'] = 'title';
    $handler->display->display_options['fields']['field_rei_user_name']['settings'] = array(
        'format' => 'last_first',
        'markup' => 0,
        'output' => 'default',
        'multiple' => 'default',
        'multiple_delimiter' => ', ',
        'multiple_and' => 'text',
        'multiple_delimiter_precedes_last' => 'never',
        'multiple_el_al_min' => '3',
        'multiple_el_al_first' => '1',
    );
    /* Sort criterion: Name Components: Family */
    $handler->display->display_options['sorts']['field_rei_user_name_family']['id'] = 'field_rei_user_name_family';
    $handler->display->display_options['sorts']['field_rei_user_name_family']['table'] = 'field_data_field_rei_user_name';
    $handler->display->display_options['sorts']['field_rei_user_name_family']['field'] = 'field_rei_user_name_family';
    /* Sort criterion: Name Components: Given */
    $handler->display->display_options['sorts']['field_rei_user_name_given']['id'] = 'field_rei_user_name_given';
    $handler->display->display_options['sorts']['field_rei_user_name_given']['table'] = 'field_data_field_rei_user_name';
    $handler->display->display_options['sorts']['field_rei_user_name_given']['field'] = 'field_rei_user_name_given';

    /* Display: Entity Reference */
    $handler = $view->new_display('entityreference', 'Entity Reference', 'entityreference_1');
    $handler->display->display_options['defaults']['title'] = FALSE;
    $handler->display->display_options['pager']['type'] = 'none';
    $handler->display->display_options['pager']['options']['offset'] = '0';
    $handler->display->display_options['defaults']['style_plugin'] = FALSE;
    $handler->display->display_options['style_plugin'] = 'entityreference_style';
    $handler->display->display_options['style_options']['search_fields'] = array(
        'field_rei_user_name' => 'field_rei_user_name',
    );
    $handler->display->display_options['defaults']['style_options'] = FALSE;
    $handler->display->display_options['defaults']['row_plugin'] = FALSE;
    $handler->display->display_options['row_plugin'] = 'entityreference_fields';
    $handler->display->display_options['row_options']['inline'] = array(
        'field_rei_user_name' => 'field_rei_user_name',
    );
    $handler->display->display_options['row_options']['hide_empty'] = TRUE;
    $handler->display->display_options['defaults']['row_options'] = FALSE;
    $views['rei_custodian_filter'] = $view;

    return $views;
}
