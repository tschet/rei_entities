# Research Equipment Inventory (REI) Entities #
This module will generate user fields and an inventory content type with fields.
It will also generate a "departments" taxonomy if one doesn't exist.

This is intended for use with the Leeds Classification module, but another 
classification taxonomy could be easily used with a few modifications.

## REQUIREMENTS  ##
  * [Leeds Classification module](https://github.com/tschet/leeds_classification), 
  though it could be replaced by another classification system.

## INSTALLATION ##
Install as usual, see http://drupal.org/node/895232 for further information.
 
## CONFIGURATION ##
There are no specific configuration settings for the REI Entities.
 
## CONTACT ##
Current maintainer:
Douglas Tschetter - [tschet](https://www.drupal.org/u/tschet)
[Riven Design](http://rivendesign.com)

## SPONSOR ##
This project was sponsored the [NDSU Research and Tech Park](http://ndsuresearchpark.com).